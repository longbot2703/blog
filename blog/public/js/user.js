function loadUserDetail(data) {


    var thiss = data.closest('tr');
    var id = data.attr('id');
    var name = thiss.children('.name').text();
    var role = thiss.children('.role').text();

    $('#txt-name').val(name);
    $('#txt-role').val(role);

    $('#userDetail').modal('show');

}

function delUser(id) {
    swal({
        title: "Are you sure?",
        text: "",
        icon: "warning",
        buttons: ['Cancel', 'OK']
    }).then((sure) => {
        if (sure) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "user_management/destroy",
                data: {
                    id
                },
                type: "delete",
                success: function (res) {
                    if (res.status == 1) {
                        swal({
                            title: res.message, text: "", icon: "success"
                        }).then((success) => {
                            location.reload()
                        })
                    } else {
                        swal({
                            title: res.message, text: "", icon: "error",
                        })
                    }
                }
            })
        }
    })
}
